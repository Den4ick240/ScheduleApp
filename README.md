# ScheduleApp
This application is for creating a personalized schedule. It allows to display,
create, change and delete events in your schedule. An event has a name,
description, place, start and end time. An event can be repeating, it can
repeat every day, every week, every two week, every month every year.

ScheduleApp also allows to import schedule from ics file url. You can also
import schedule from official Novosibirsk State University web-site by
providing your group number.

The app also provides notifications and alarms before events.

The application is **written in java**. The application has a [java submodule](domain)
that contains the buisness logic and has a **high test coverage** 


### Event list

Examples of event list screen.

![Alt text](/screenshots/schedule1.jpg)

![Alt text](/screenshots/schedule2.jpg)

### Create event

Screen for creating an event.

![Alt text](/screenshots/create.jpg)

### Import schedule screen

Screen for importing schedule from ics file url or from group number.

![Alt text](/screenshots/import.jpg)

### Navigation drawer

Screenshot of navigation drawer.

![Alt text](/screenshots/drawer.jpg)

### Settings screen

Settings screen allows to toggle notifications and alarms for all events.

![Alt text](/screenshots/settings.jpg)

